### Instructor Services Summary

The following document contains information about the core services for getting and editing data related to an instructor.

#### Get instructor classes

Returns a collection of course classes where the current user is an instructor.

GET /api/instructors/me/classes

    /**
     * @type {Array<CourseClass>}
     */
    let classes = await context.model('instructors/me/classes')
        .where('year').equal(2015)
        .and('period').equal(1)
        .expand('period')
        .getItems();

#### Get instructor classes of current academic period

Returns a collection of course classes of the current academic period where the current user is an instructor.

    GET /api/instructors/me/currentClasses
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept: application/json
    Accept-Language: el

where authorization header contains user access token received from OAuth2 authentication server,
accept header defines an acceptable mime type (application/json) and accept-language header is the preferred locale 
for this request.

    /**
     * @type {Array<CourseClass>}
     */
    let classes = await context.model('instructors/me/currentClasses')
        .expand('period')
        .getItems();

#### Get instructor course exams

Returns a collection of course exams where the current user is an instructor or supervisor.

    GET /api/instructors/me/classes
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept: application/json
    Accept-Language: el
 
where authorization header contains user access token received from OAuth2 authentication server,
accept header defines an acceptable mime type (application/json) and accept-language header is the preferred locale 
for this request.

    /**
     * @type {Array<CourseExam>}
     */
    let exams = await context.model('instructors/me/exams')
        .where('year').equal(2015)
        .expand('examPeriod', 'course')
        .orderByDescending('examPeriod')
        .getItems();
 
 #### Get instructor course exams of current academic period
 
Returns a collection of course exams of the current academic period where the current user is an instructor or supervisor.
 
    GET /api/instructors/me/currentExams
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept: application/json
    Accept-Language: el
 
where authorization header contains user access token received from OAuth2 authentication server,
accept header defines an acceptable mime type (application/json) and accept-language header is the preferred locale 
for this request.
 
     /**
      * @type {Array<CourseExam>}
      */
     let exams = await context.model('instructors/me/exams')
         .expand('examPeriod', 'course')
         .orderByDescending('examPeriod')
         .getItems();
         
#### Get instructor class students

Returns a collection of students who had been registered in course classes where the current user was an instructor.

    GET /api/instructors/me/classes/students
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept: application/json
    Accept-Language: el

where authorization header contains user access token received from OAuth2 authentication server,
accept header defines an acceptable mime type (application/json) and accept-language header is the preferred locale 
for this request.

    /**
     * @type {Array<StudentCourseClass>}
     */
    let students = await context.model('instructors/me/classes/students')
        .where('student/person/familyName').contains('RICHAR')
        .expand('student', 'courseClass($expand=course)')
        .getItems();
        
#### Send a message to student for a specific courseClass

Send a message to a student for a registered course class

    POST /api/instructors/me/classes/{courseClass}/students/{student}/sendMessage
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Content-Type: multipart/form-data
    Accept-Language: el
    
where authorization header contains user access token received from OAuth2 authentication server,
content-type defines a multipart/form-data request and accept-language header is the preferred locale 
for this request.

Form data can contain a field called "file" which is the file that is going to be uploaded as message attachment.
      
           POST /api/instructors/me/classes/123/students/456/sendMessage HTTP/1.1
             Host: localhost:5001
             Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
             Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
             cache-control: no-cache
             Postman-Token: 34a0b0cc-dd53-425c-9a86-a7dbefed0161
             
             Content-Disposition: form-data; name="file"; filename="C:\Projects\universis\universis-students\src\assets\img\universis_logo_128_color.png
             
             
             
             Content-Disposition: form-data; name="body"
             
             Welcome
             
             Content-Disposition: form-data; name="subject"
             
             Test
             ------WebKitFormBoundary7MA4YWxkTrZu0gW--

The response of this operation is a StudentMessage object:

    {
        "subject": "lorem ipsum",
        "body": "This is a test message",
        "description": "rem ipsum dolor sit amet, consectetur adi",
        "student": 12345,
        "recipient": 222,
        "sender": 111,
        "owner": 111,
        "identifier": "120596E8-8F0B-4DB4-A09B-40FADF02240A",
        "dateCreated": "2019-11-25T09:36:18.362Z",
        "dateModified": "2019-11-25T09:36:18.972Z",
        "createdBy": 111,
        "modifiedBy": 111,
        "id": 512,
        "attachments": [
                {
                    "contentType": "application/pdf",
                    "owner": 111,
                    "version": 1,
                    "published": false,
                    "alternateName": "mF123MCxRbem",
                    "url": "\\api\\content\\private\\m123qMCxRbem",
                    "additionalType": "Attachment",
                    "dateCreated": "2019-11-25T11:41:51.842Z",
                    "dateModified": "2019-11-25T11:41:51.882Z",
                    "createdBy": 111,
                    "modifiedBy": 111,
                    "id": 1281
                }
            ]
    }
            
            
 #### Send a message to student if instructor is student's thesis supervisor
     
     Send a message to a student if instructor is the supervisor of student's thesis
     
         POST /api/instructors/me/theses/{thesis}/students/{student}/sendMessage
         Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
         Content-Type: multipart/form-data
         Accept-Language: el
         
     where authorization header contains user access token received from OAuth2 authentication server,
     content-type defines a multipart/form-data request and accept-language header is the preferred locale 
     for this request.
     
     Form data can contain a field called "file" which is the file that is going to be uploaded as message attachment.
           
                POST /api/instructors/me/theses/125/students/456/sendMessage HTTP/1.1
                  Host: localhost:5001
                  Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
                  Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
                  cache-control: no-cache
                  Postman-Token: 34a0b0cc-dd53-425c-9a86-a7dbefed0161
                  
                  Content-Disposition: form-data; name="file"; filename="C:\Projects\universis\universis-students\src\assets\img\universis_logo_128_color.png
                  
                  
                  
                  Content-Disposition: form-data; name="body"
                  
                  Welcome
                  
                  Content-Disposition: form-data; name="subject"
                  
                  Test
                  ------WebKitFormBoundary7MA4YWxkTrZu0gW--
     
     The response of this operation is a StudentMessage object:
     
         {
             "subject": "lorem ipsum",
             "body": "This is a test message",
             "description": "rem ipsum dolor sit amet, consectetur adi",
             "student": 12345,
             "recipient": 222,
             "sender": 111,
             "owner": 111,
             "identifier": "120596E8-8F0B-4DB4-A09B-40FADF02240A",
             "dateCreated": "2019-11-25T09:36:18.362Z",
             "dateModified": "2019-11-25T09:36:18.972Z",
             "createdBy": 111,
             "modifiedBy": 111,
             "id": 512,
             "attachments": [
                     {
                         "contentType": "application/pdf",
                         "owner": 111,
                         "version": 1,
                         "published": false,
                         "alternateName": "mF123MCxRbem",
                         "url": "\\api\\content\\private\\m123qMCxRbem",
                         "additionalType": "Attachment",
                         "dateCreated": "2019-11-25T11:41:51.842Z",
                         "dateModified": "2019-11-25T11:41:51.882Z",
                         "createdBy": 111,
                         "modifiedBy": 111,
                         "id": 1281
                     }
                 ]
         }
            
#### Get instructor thesis students

Returns a collection of students who had been assigned theses where the current user was supervisor.

    GET /api/instructors/me/theses/students
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept: application/json
    Accept-Language: el
 
where authorization header contains user access token received from OAuth2 authentication server,
accept header defines an acceptable mime type (application/json) and accept-language header is the preferred locale 
for this request.

    /**
     * @type {Array<StudentThesis>}
     */
    let students = await context.model('instructors/me/theses/students')
        .where('student/person/familyName').contains('RICHAR')
        .expand('student', 'thesis')
        .getItems();

#### Get course exam students

Returns a collection of students who are eligible to participate in a course exam.

    GET /api/instructors/me/exams/{courseExam}/students
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept: application/json
    Accept-Language: el
    
where authorization header contains user access token received from OAuth2 authentication server,
accept header defines an acceptable mime type (application/json) and accept-language header is the preferred locale 
for this request.

    let courseExam = ...;
    /**
     * @type {Array<CourseExamStudentGrade>}
     */
    let students = await context.model(`instructors/me/exams/${courseExam}/students`)
        .asQueryable()
        .orderBy('student/person/familyName')
        .thenBy('student/person/givenName')
        .expand('student($expand=studentStatus)')
        .getItems();

Result:

    [
        {
            "id": "62D7D79E-A8FF-4224-A568-B628167AA7F3",
            "student": {
                "id": 20025246,
                "studentIdentifier": "100126230",
                "department": 170,
                "familyName": "Richardson",
                "givenName": "Alexis",
                "fatherName": "Peter",
                "inscriptionYear": 2014,
                "inscriptionPeriod": 1,
                "studentStatus": {
                    "id": 1,
                    "additionalType": null,
                    "alternateName": "active",
                    "description": "Active student",
                    "name": "Active",
                    ...
                }
                "semester": 9
            },
            "registrationSemester": 9,
            "courseExam": 20095565,
            "courseClass": 20054916,
            "registrationType": 1,
            ...
            "grade1": null,
            "grade2": null,
            "examGrade": null,
            "formattedGrade": null,
            "dateModified": null
        }
        ...
    ]

#### Export course class students

Returns an *.xlsx or *.csv which contains a collection of students who have been registered to the given course class.

    GET /api/instructors/me/classes/{courseClass}/students/export
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    Accept-Language: el

where authorization header contains user access token received from OAuth2 authentication server,
accept header defines an acceptable mime type (text/csv for *.csv or  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet for *.xlsx) and accept-language header is the preferred locale 
for this request.

#### Export course exam students

Returns an *.xlsx or *.csv which contains a collection of students who are eligible to participate in a course exam.

    GET /api/instructors/me/exams/{courseExam}/students/export
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    Accept-Language: el

where authorization header contains user access token received from OAuth2 authentication server,
accept header defines an acceptable mime type (text/csv for *.csv or  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet for *.xlsx) and accept-language header is the preferred locale 
for this request.



#### Import course exam students

Imports a collection of students grades of a course exam by uploading a *.csv or *.xlsx file

    POST /api/instructors/me/exams/{courseExam}/students/import
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Content-Type: multipart/form-data
    Accept-Language: el
    
where authorization header contains user access token received from OAuth2 authentication server,
content-type defines a multipart/form-data request and accept-language header is the preferred locale 
for this request.

Form data should contain a field called "file" which is the file (*.csv or *.xlsx) that is going to be uploaded.
      
      curl -X POST \
        http://localhost:5001/api/instructors/me/exams/20111234/students/import \
        -H 'accept-language: el' \
        -H 'authorization: Bearer 34417a537741525365376f76546f5048586761367251454c' \
        -H 'cache-control: no-cache' \
        -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
        -F 'file=@StudentGrades1.xlsx'

The response of this operation is ExamDocumentUploadAction object:

    {
        "object": {
            "id": 20115391,
            "course": 20004993,
            "year": {
                "id": 2017,
                "alternateName": "2017-2018",
                ...
            },
            "examPeriod": 20000002,
            "description": "Course A 2017-2018 June",
            ...
            "gradeScale": 20000000,
            "grades": [
                {
                    "courseExam": 20115391,
                    "student": 20061234,
                    "studentIdentifier": "100",
                    "familyName": "Richardson",
                    "givenName": "Alexis",
                    "fatherName": "Peter",
                    "studentStatus": "Active",
                    "inscriptionYear": 2013,
                    "registrationSemester": 10,
                    "formattedGrade": "5",
                    "examGrade": 0.5,
                    "grade1": 0.5,
                    "validationResult": {
                        "success": true,
                        "statusCode": 200,
                        "code": "UNMOD",
                        "message": "Grade has not been modified."
                    }
                },
                {
                    "courseExam": 20115391,
                    "student": 20065086,
                    "studentIdentifier": "101",
                    "familyName": "Williams",
                    "givenName": "Joan",
                    "fatherName": "Michael",
                    "studentStatus": "Active",
                    "inscriptionYear": 2013,
                    "registrationSemester": 8,
                    "formattedGrade": "6",
                    "validationResult": {
                        "success": false,
                        "statusCode": 200,
                        "code": "INS",
                        "message": "A new student grade will be inserted."
                    }
                }
                ...
            ],
            "validationResult": {
                "success": true,
                "statusCode": 200,
                "code": "SUCC",
                "message": "Course exam validation check was completed successfully."
            }
        },
        "additionalType": "ExamDocumentUploadAction",
        "actionStatus": {
            "alternateName": "ActiveActionStatus",
            "id": 4
        },
        "owner": 20018003,
        "startTime": "2019-01-17T19:43:48.686Z",
        "code": "udAaaZBFTbkubzU5gUkVwvUoaQLwLQwu",
        "dateCreated": "2019-01-17T19:43:48.692Z",
        "dateModified": "2019-01-17T19:43:48.719Z",
        "createdBy": 1234,
        "modifiedBy": 1234,
        "id": 135
    }

where ExamDocumentUploadAction.object property contains information about the target course exam and validation results for each student grade.

ExamDocumentUploadAction.object.validationResult property may be:

| Code  | Success  | Title           | Description |
|-------|----------|-----------------|---|
| SUCC  | true     | Success         | Course exam validation check was completed successfully. |
| PSUCC | true     | Partial Success | Course exam contains warnings-errors. |
| EFAIL | false    | Failure         | Course exam cannot be validated due to internal error (or any other more specific message) |

Each student grade has also a validation result which may be one of the following:

| Code  | Success  | Title           | Description |
|-------|----------|-----------------|--- |
| INS   | true     | Insert          | A new student grade will be inserted. |
| UPD   | true     | Update          | Student grade already exists and it will be replaced. |
| UNMOD | true     | Unmodified      | Grade has not been modified. |
| EAVAIL| false    | Not Available   | The student specified is unavailable for the selected course exam. |
| EFAIL | false    | Failure         | An internal server error occurred. |
| ERANGE| false    | Out Of Range    | Out of range value for grade |

**Important Note**: Import operation does not any student grade. The final commit should be done by completing a document upload action.

#### Complete a document upload action

Completes an active document upload action and commits student grades to the specified course exam.

    POST /api/instructors/me/exams/{courseExam}/actions/{action}/complete
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept-Language: el
        
where authorization header contains user access token received from OAuth2 authentication server 
and accept-language header is the preferred locale for this request.

The result of this operation is the completed document upload action. 
It contains a validation result for each student that has been inserted or failed.
It also contains a result of type CourseExamDocument with a checkHashKey and an original xml which contains only the modified grades.  

    {
        "additionalType": "ExamDocumentUploadAction",
        "actionStatus": {
            "alternateName": "CompletedActionStatus",
            "id": 3
        },
        "owner": 20018003,
        ...
        "result": {
                "id": 12,
                "courseExam": 20115391,
                "user": 20018003,
                "documentSubject": "Upload exam for Course A 2017-2018 June",
                "originalDocument": "<?xml version=\"1.0\"?><ExamGrades>...</ExamGrades>",
                "signedDocument": null,
                "signatureBlock": null,
                "userCertificate": null,
                "documentStatus": 1,
                "documentStatusReason": "In progress",
                "checkHashKey": "nzdsZG+aaj3Dn23a25beTK+VMd23=",
                "dateCreated": "...",
                "dateModified": "..."
            },
        "object": {
            "id": 20115391,
            "course": 20004993,
            "year": {
                "id": 2017,
                "alternateName": "2017-2018",
                ...
            },
            "examPeriod": 20000002,
            "description": "Course A 2017-2018 June",
            ...
            "gradeScale": 20000000,
            "status": {
                "id": 3,
                "additionalType": null,
                "alternateName": "inprogress",
            },
            "grades": [
                {
                   "courseExam": 20115391,
                   "student": 20061234,
                   "studentIdentifier": "100",
                   "familyName": "Richardson",
                   "givenName": "Alexis",
                   "fatherName": "Peter",
                   "studentStatus": "Active",
                   "inscriptionYear": 2013,
                   "registrationSemester": 10,
                   "formattedGrade": "5",
                   "examGrade": 0.5,
                   "grade1": 0.5,
                    "validationResult": {
                        "success": true,
                        "statusCode": 200,
                        "code": "NOACTION",
                        "message": "'Student grade is not supplied or is the same."
                    }
                },
                {
                    "courseExam": 20115391,
                    "student": 20065086,
                    "studentIdentifier": "101",
                    "familyName": "Williams",
                    "givenName": "Joan",
                    "fatherName": "Michael",
                    "studentStatus": "Active",
                    "inscriptionYear": 2013,
                    "registrationSemester": 8,
                    "formattedGrade": "6",
                    "validationResult": {
                        "success": true,
                        "statusCode": 200,
                        "code": "SUCC",
                        "message": "Student grade successfully saved."
                    }
                },
                {
            ],
            "validationResult": {
                "success": true,
                "statusCode": 200,
                "code": "SUCC",
                "message": "Course exam successfully saved."
            }
        }
    }

ExamDocumentUploadAction.object.validationResult property may be:

| Code  | Success  | Title           | Description |
|-------|----------|-----------------|---|
| SUCC  | true     | Success         | Course exam successfully saved. |
| PSUCC | true     | Partial Success | Course exam successfully saved but with errors. |
| EFAIL | false    | Failure         | Course exam cannot be saved. (or any other more specific message) |

Each student grade has also a validation result which may be one of the following:

| Code     | Success  | Title           | Description |
|-------   |----------|-----------------|--- |
| NOACTION | true     | No Action       | Student grade is not supplied or is the same. |
| INVDATA  | false    | Invalid Data    | The specified grade is not valid. (or any other more specific message) |
| FAIL     | false    | Failed          | An internal error occurred while saving grade. |
| SUCC     | true     | Success         | Student grade successfully saved. |

#### Cancel a document upload action

Cancels an active document upload action associated the the specified the specified course exam.

    POST /api/instructors/me/exams/{courseExam}/actions/{action}/cancel
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept-Language: el
        
where authorization header contains user access token received from OAuth2 authentication server 
and accept-language header is the preferred locale for this request.

#### Get exam document upload actions

Returns a collection of exam document upload actions

    GET /api/instructors/me/exams/{courseExam}/actions
    Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
    Accept-Language: el
        
where authorization header contains user access token received from OAuth2 authentication server 
and accept-language header is the preferred locale for this request.

    let courseExam = ...;
    /**
     * @type {Array<ExamDocumentUploadAction>}
     */
    let documents = await context.model(`instructors/me/exams/${courseExam}/actions`)
        .where('actionStatus/alternateName').equal('CompletedActionStatus')
        .expand('attachments', 'result')
        .orderByDescending('dateCreated')
        .take(5)
        .getItems();



        