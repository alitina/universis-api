import swaggerJSDoc from 'swagger-jsdoc';
import path from "path";
import {Url} from "url";
let npmPackage = require(path.resolve(process.cwd(),'package.json'));

/**
 *
 * @param {IApplication} app
 */
function getSwaggerDefinition(app) {

    const swaggerDefinition = {
        "openapi": "3.0.0",
        "info": {
            "description": npmPackage.description,
            "version": npmPackage.version,
            "title": npmPackage.name,
            "termsOfService": "https://example.com/terms/",
            "contact": {
                "email": "support@example.com"
            },
            "license": {
                "name": "Apache 2.0",
                "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
            }
        },
        "paths": {},
        "components": {
            "schemas": {

            },
            "securitySchemes": {
                "OAuth2": {
                    "type": "oauth2",
                    "description": "For more information, see https://api.slack.com/docs/oauth",
                    "flows": {
                        "authorizationCode": {
                            authorizationUrl: new URL("authorize", app.getConfiguration().getSourceAt('settings/auth/server_uri')),
                            tokenUrl: new URL("access_token", app.getConfiguration().getSourceAt('settings/auth/server_uri')),
                            "scopes": {
                                "students": "Read and write student information",
                                "teachers": "Read and write instructor information"
                            }
                        }
                    }
                }
            }
        }
    };

    const options = {
        swaggerDefinition,
        apis: [path.resolve(process.cwd(),'server/routes/*.js')]
    };
    return swaggerJSDoc(options);
}

module.exports.getSwaggerDefinition = getSwaggerDefinition;