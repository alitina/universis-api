import { LangUtils, RandomUtils } from '@themost/common/utils';
import {DataModel} from '@themost/data/data-model';
import {DataModelView} from "@themost/data/data-model-view";
import _ from 'lodash';
import moment from 'moment';
import {SchemaLoaderStrategy} from "@themost/data";

const resolver = {
    today:function() {
        let callback;
        if (arguments.length === 0) {
            return moment().startOf('day').toDate();
        }
        else if (arguments.length === 1) {
            callback = arguments[0];
        }
        else {
            callback = arguments[1];
        }
        callback(null, moment().startOf('day').toDate());
    },
    lastMonth:function(callback)
    {
        callback(null,moment().subtract(1, 'month').toDate());
    },
    lastWeek:function(callback)
    {
        callback(null, moment().subtract(7, 'day').toDate());
    },
    lastDay:function(callback)
    {
        callback(null, moment().subtract(1, 'day').toDate());
    },
    lastHour:function(callback)
    {
        callback(null, moment().subtract(1, 'day').toDate());
    },
    me:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this, undefinedUser = 0;
        if (self.context) {
            const user =self.context.interactiveUser || self.context.user || { name:'anonymous' };
            if (_.isNil(user.id)) {
                //get user id
                const users = self.context.model('User');
                users.where('name').equal(user.name).select('id').flatten().silent().first(function(err, result) {
                    if (err) {
                        callback(err);
                    }
                    else {
                        if (_.isNil(result)) {
                            callback(null, undefinedUser);
                        }
                        else {
                            callback(null, result.id);
                        }
                    }
                });
            }
            else {
                callback(null, user.id);
            }
        }
        else {
            callback(null, undefinedUser);
        }
    },
    student:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this, undefinedStudent = 0;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User'), students = self.context.model('Student');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            students.join('User').where(users.fieldOf('name')).equal(username).select([students.fieldOf(students.primaryKey)]).flatten().silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, undefinedStudent);
                    }
                    else {
                        //get student user
                        callback(null, result[students.primaryKey]);
                    }
                }
            });
        }
        else {
            callback(null, undefinedStudent);
        }
    },
    department:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            users.where('name').equal(username).select(['id','departments']).expand('departments').silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, 0);
                    }
                    else if (_.isNil(result['departments'][0])) {
                        callback(null, 0);
                    }
                    else {
                        //get user department
                        callback(null, result['departments'][0].id);
                    }
                }
            });
        }
        else {
            callback(null, 0);
        }
    },
    currentYear:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            users.where('name').equal(username).select(['id','departments']).expand('departments').silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, 0);
                    }
                    else if (_.isNil(result['departments'][0])) {
                        callback(null, 0);
                    }
                    else {
                        callback(null, LangUtils.parseInt(result['departments'][0].currentYear));
                    }
                }
            });
        }
        else {
            callback(null, 0);
        }
    },
    currentPeriod:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            users.where('name').equal(username).select(['id','departments']).expand('departments').silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, 0);
                    }
                    else if (_.isNil(result['departments'][0])) {
                        callback(null, 0);
                    }
                    else {
                        callback(null, LangUtils.parseInt(result['departments'][0].currentPeriod));
                    }
                }
            });
        }
        else {
            callback(null, 0);
        }
    }
    ,
    instructor:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this, undefinedInstructor = -(RandomUtils.randomInt(1,1000000));
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const instructors = self.context.model('Instructor');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            instructors.where("user/name").equal(username).select(['id']).flatten().silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, undefinedInstructor);
                    }
                    else {
                        //get instructor user
                        callback(null, result['id']||undefinedInstructor);
                    }
                }
            });
        }
        else {
            callback(null, undefinedInstructor);
        }
    },
    program:function(callback) {
        /**
         * @type {DataModel|*}
         */
        const self = this;
        if (self.context) {
            const user = self.context.user || { name:'anonymous' };
            const users = self.context.model('User'), students = self.context.model('Student');
            let username=user.name;
            if (self.context.interactiveUser && self.context.interactiveUser.name)
                username=self.context.interactiveUser.name;
            students.join('User').where(users.fieldOf('name')).equal(username).select([students.fieldOf('studyProgram')]).flatten().silent().first(function(err, result) {
                if (err) {
                    callback(err);
                }
                else {
                    if (_.isNil(result)) {
                        callback(null, 0);
                    }
                    else {
                        //get student user
                        callback(null, result['studyProgram']);
                    }
                }
            });
        }
        else {
            callback(null, 0);
        }
    },
    departments: function (callback) {
        const undefinedDepartment = -1;

        if (_.isNil(this.context.user)) {
            return callback(null, undefinedDepartment);
        }

        return this.context.model('User')
            .where('name')
            .equal(this.context.user.name)
            .select('id', 'departments')
            .expand('departments')
            .silent()
            .getItem()
            .then((res) => {
                if (res && res.departments && res.departments.length > 0) {
                    return callback(null, _.map(res.departments, function (x) {
                        return x.id;
                    }));
                }
                return callback(null, undefinedDepartment);
            }).catch((err) => {
                return callback(err);
            })
    },
     lang:  function(callback) {
        let culture = this.context.locale;
        if (culture) {
            return callback(null, culture.substr(0,2));
        }
        else {
            return callback(null, "en");
        }
    }
};

/**
 * @name DataModel~resolveMethod
 * @param {string} name
 * @param {*} args
 * @param {Function} callback
 *
 */

/**
 * A memoized extender of DataModel.prototype.getReferenceMappings(deep) method
 * @param {DataModel} thisModel
 * @param {boolean=} deep
 * @returns {Promise<Array<any>>}
 */
const getReferenceMappingsFunc = _.memoize(function getModelReferenceMappings(thisModel, deep) {
    try {
        // init collection of mapping
        const referenceMappings = [];
        // get model name
        const thisName = thisModel.name;
        // get configuration
        const configuration = thisModel.context.getConfiguration();
        /**
         * @type {SchemaLoaderStrategy}
         */
        const schemaLoader = configuration.getStrategy(SchemaLoaderStrategy);
        // get all models
        const names = schemaLoader.getModels();
        // enumerate models
        names.forEach( name => {
            // get model
            const model = thisModel.context.model(name);
            let attributes = [];
            if (model) {
                // if deep is true
                if (deep) {
                    // get attributes even if they are inherited
                    attributes = model.attributes;
                } else {
                    // otherwise filter only attributes belongs to this model
                    // (exclude inherited attributes)
                    attributes = model.attributes.filter( a => {
                        return (a.model === model.name || a.cloned === true);
                    });
                }
            }
            attributes.forEach( attribute => {
                const mapping = model.inferMapping(attribute.name);
                if (mapping &&
                    ((mapping.parentModel === thisName) ||
                        (mapping.childModel === thisName && mapping.associationType === 'junction'))
                ) {
                    // push reference
                    referenceMappings.push(Object.assign({}, mapping, { refersTo: attribute.name }));
                }
            });
        });
        return Promise.resolve(referenceMappings);
    }
    catch (err) {
        return Promise.reject(err);
    }
}, (thisModel, deep) => {
    // return custom key e.g. Department.getReferenceMappings(true)
    return `${thisModel && thisModel.name}.getReferenceMappings(${deep})`;
});

_.assign(DataModel.prototype, {
    /**
     * @this DataModel
     * @param {boolean=} deep
     * @returns Promise<Array<any>>
     */
    getReferenceMappings: function(deep) {
        return getReferenceMappingsFunc(this, !!deep);
    },
    /**
     * @param {string} name
     * @param {*} args
     * @param {Function} callback
     */
    resolveMethod: function(name, args, callback) {
        if (typeof resolver[name] === 'function') {
            const args1 = args || [];
            args1.push(callback);
            resolver[name].apply(this, args1);
        }
        else {
            callback();
        }
    }
});

// export getReferenceMappingsFunc.cache as DataModel.prototype.getReferenceMappings.cache
// in order to allow managing cache
Object.defineProperty(DataModel.prototype. getReferenceMappings, 'cache', {
    enumerable: true,
    configurable: true,
    get() {
        return getReferenceMappingsFunc.cache;
    }
});

_.assign(DataModelView.prototype, {
    /**
     * @method
     * @name DataModelView~toLocaleArray
     * @param {Array<*>} arr
     * @returns {Array<*>}
     */
    toLocaleArray: function(arr) {
        const self = this;
        if (Array.isArray(arr)) {
            // map attributes
            let attributes = this.attributes.map( attribute=> {
                // get title
               let title = attribute.title || attribute.getName();
               return {
                   name: attribute.getName(),
                   title: self.model.context.__(title)
               }
            });
            return arr.map( item => {
                return attributes.reduce( (obj, attribute) => {
                    // set value
                    obj[attribute.title] = item[attribute.name];
                    return obj;
                }, { });
            });
        }
        throw new TypeError('Expected array of objects');
    },
    /**
     * @method
     * @name DataModelView~fromLocaleArray
     * @param {Array<*>} arr
     * @returns {Array<*>}
     */
    fromLocaleArray: function(arr) {
        const self = this;
        if (Array.isArray(arr)) {
            // map attributes
            let attributes = this.attributes.map( attribute=> {
                // get title
                let title = attribute.title || attribute.getName();
                return {
                    name: attribute.getName(),
                    title: self.model.context.__(title)
                }
            });
            return arr.map( item => {
                return attributes.reduce( (obj, attribute) => {
                    // set value
                    obj[attribute.name] = item[attribute.title] ?? item[attribute.name];
                    return obj;
                }, { });
            });
        }
        throw new TypeError('Expected array of objects');
    }

});
