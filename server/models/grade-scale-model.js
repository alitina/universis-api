import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {round} from 'mathjs';
import {Args} from "@themost/common/utils";
const decimalSeparatorRegExp = Symbol('decimalSeparatorRegExp');
/**
 * @class
 
 * @property {number} [id]
 * @property {string} name
 * @property {number} scaleType
 * @property {number} scaleFactor
 * @property {number} scaleBase
 * @property {number} formatPrecision
 * @property {number} scalePrecision
 * @property {Array<GradeScaleValue|any>} values
 * @extends DataObject
 */
@EdmMapping.entityType('GradeScale')
class GradeScale extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
        Object.defineProperty(this, 'decimalSeparator', {
           get: function() {
               if (this[decimalSeparatorRegExp]) {
                   return this[decimalSeparatorRegExp];
               }
               let format = (0.1).toLocaleString(this.context.locale);
               this[decimalSeparatorRegExp] = new RegExp('\\' + format.substr(1,1),'ig');
               return this[decimalSeparatorRegExp];
           }
        });
    }

    /**
     * Converts the given value to the equivalent grade value based on this grade scale
     * @param {*} value
     * @returns number|*
     */
    convertFrom(value) {
        if (value === null || typeof value === 'undefined')
            return;
        // numeric grade scale
        if (this.scaleType === 0) {
            let finalGrade;
            // if grade is a number
            if (typeof value === 'number') {
                finalGrade = value;
            }
            else if (typeof value === 'string') {
                // try to convert the given grade
                finalGrade = parseFloat(value.replace(this.decimalSeparator,'.'));
                if (isNaN(finalGrade)) {
                    return;
                }
            }
            if (typeof this.scaleFactor !== 'number') {
                throw new TypeError('Grade scale factor must be a number.');
            }
            if (this.scaleFactor<=0) {
                throw new TypeError('Grade scale factor must greater than zero.');
            }
            // validate grade
            let res = round((finalGrade * this.scaleFactor), this.formatPrecision +1);
            //throw error if result is greater than 1
            if (res<0 || res>1) {
                throw new RangeError('Out of range value for grade');
            }
            return res;
        }
        else if (this.scaleType === 1 || this.scaleType === 3) {
            let findValue = this.values.find( x => {
                return x.name === value || x.alternateName === value;
            });
            if (findValue) {
                return findValue.exactValue;
            }
            throw new RangeError('Out of range value for grade');
        }
        throw new TypeError('Invalid grade scale type.');
    }

    /**
     * Converts the given value to the equivalent grade value based on this grade scale
     * @param {number} value
     * @param {Boolean=} holdFractionDigits
     * @returns string|*
     */
    convertTo(value, holdFractionDigits) {
        // numeric grade scale
        if (this.scaleType === 0) {
            // if grade is a number
            if (typeof value === 'undefined' || value === null) {
                return;
            }
            // validate value type
            Args.notNumber(value, 'Grade');
            // validate value range
            Args.check(value>=0 && value<=1, new RangeError('Out of range value for grade'));
            if (typeof this.scaleFactor !== 'number') {
                throw new TypeError('Grade scale factor must be a number.');
            }
            if (this.scaleFactor<=0) {
                throw new TypeError('Grade scale factor must greater than zero.');
            }
            return (value / this.scaleFactor).toLocaleString(this.context.locale, {
                minimumFractionDigits: holdFractionDigits ? this.formatPrecision : 0,
                maximumFractionDigits: this.formatPrecision
            });
        }
        else if (this.scaleType === 1 || this.scaleType === 3) {
            let finalValue = round(value, this.scalePrecision);
            let findValue = this.values.find( x => {
                return finalValue >= x.valueFrom && finalValue <= x.valueTo;
            });
            if (findValue) {
                return findValue.name;
            }
            throw new RangeError('Out of range value for grade');
        }
        throw new TypeError('Invalid grade scale type.');
    }

}
module.exports = GradeScale;
