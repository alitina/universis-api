import {EdmMapping,EdmType} from '@themost/data/odata';
import _ from 'lodash';
import {DataObject} from "@themost/data/data-object";
import {DataNotFoundError} from "@themost/common/errors";

@EdmMapping.entityType("Department")
/**
 * @class
 * @augments DataObject
 */
class Department extends DataObject {

    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.func("ActivePrograms",EdmType.CollectionOf("StudyProgram"))
    getActivePrograms() {
        return this.context.model('StudyProgram').where('department').equal(this.id).prepare();
    }

    /**
     * @param {number} year
     * @param {number} period
     * @returns {DataQueryable}
     */
    @EdmMapping.param('period', EdmType.EdmInt32, false)
    @EdmMapping.param('year', EdmType.EdmInt32, false)
    @EdmMapping.func("CourseClasses" ,EdmType.CollectionOf("CourseClass"))
    getCourseClasses(year, period) {
        return this.context.model('CourseClass')
            .where('course/department').equal(this.getId())
            .and('year').equal(year)
            .and('period').equal(period)
            .prepare();
    }

    @EdmMapping.func("CurrentClasses" ,EdmType.CollectionOf("CourseClass"))
    getCurrentClasses() {
        const self = this;
        return self.getModel().where('id').equal(self.id).select('currentYear','currentPeriod').getItem().then(function(result) {
            if (_.isNil(result)) {
                return Promise.reject(new DataNotFoundError("Department not found"));
            }
            return self.context.model('CourseClass')
                .where('course/department').equal(self.id)
                .and('year').equal(result.currentYear)
                .and('period').equal(result.currentPeriod)
                .prepare();
        });

    }

    /**
     * @returns {DepartmentSnapshot}
     */
    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('snapshot', 'DepartmentSnapshot')
    async createSnapshot(data) {
        /**
         * get LocalDepartment
         * @type {LocalDepartment}
         */
        let department = await this.context.model('LocalDepartment')
            .asQueryable()
            .expand('reportVariables', 'locales', 'organization')
            .where('id').equal(this.getId()).silent().getItem();

        // assign data to department
        department = Object.assign(department, data);
        // set department
        department.department = department.id;
        // set institute attributes name,alternateName, instituteType
        department.instituteName = department.instituteName || department.organization.name;
        department.instituteAlternateName = department.instituteAlternateName || department.organization.alternateName
        department.instituteType = department.instituteType || department.organization.instituteType;

        // remove attributes
        delete department.departmentConfiguration;
        delete department.id;

        // assign report variables
        if (department.reportVariables) {
            department.reportVariables = department.reportVariables.map(department => {
                delete department.id;
                return department;
            });
        }
        // assign locales
        if (department.locales) {
            department.locales = department.locales.map(locale => {
                delete locale.id;
                locale.instituteName = department.instituteName;
                locale.instituteAlternateName = department.instituteAlternateName;
                return locale;
            });
        }
        // return new snapshot
        return await this.context.model('DepartmentSnapshot').save(department);
    }

}

module.exports = Department;
