import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {string} name
 * @property {string} vatNumber
 * @property {string} vatOffice
 * @property {string} email
 * @property {string} address
 * @property {string} phone
 * @property {string} fax
 * @property {Date} dateModified
 * @augments {DataObject}
 */
@EdmMapping.entityType('Company')
class Company extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Company;