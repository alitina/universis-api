import {DataObject} from "@themost/data/data-object";
import {EdmMapping} from "@themost/data/odata";
import _ from "lodash";
import {round} from "mathjs";

@EdmMapping.entityType('StudentCourse')
/**
 * @class
 * @augments DataObject
 */
class StudentCourse extends DataObject {
    constructor() {
        super();
    }

    async getLastGrade() {
        const self = this, context = self.context;
        try {
            // get student
            const student = self.student.id || self.student;
            const courseId = self.course.id || self.course;
            // get grades for student
            const course = await context.model('Course').where('id').expand('gradeScale').equal(courseId).getItem();
            let result = {
                grade: null,
                gradeYear: null,
                gradePeriod: null,
                lastRegistrationYear: null,
                lastRegistrationPeriod: null,
                examPeriod: null
            };

            if (course.courseStructureType!==4) {
                // get latest grade
                const lastGrade = await context.model('StudentGrade').where('student').equal(student)
                    .and('courseExam/course').equal(courseId).and('status/alternateName').equal('normal')
                    .expand({
                        name: 'courseExam',
                        options: {
                            $expand: 'examPeriod'
                        }
                    }, 'courseClass')
                    .orderByDescending('courseExam/year').thenByDescending('courseExam/examPeriod').getItem();
                if (lastGrade) {
                    result.grade = lastGrade.examGrade;
                    result.gradeYear = lastGrade.courseExam.year;
                    result.gradePeriod = lastGrade.courseClass.period;
                    result.lastRegistrationYear = lastGrade.courseClass.year;
                    result.lastRegistrationPeriod = lastGrade.courseClass.period;
                    result.examPeriod = lastGrade.courseExam.examPeriod.id;
                    result.gradeExam = lastGrade.courseExam.id;
                    result.gradePeriodDescription = lastGrade.courseExam.examPeriod.alternateName.substring(0,10) // max len 10.
                }
                return result;
            }
            else {
                // get course parts and calculate final complex grade
                 // get courseParts
                const courseParts = await context.model('StudentCourse')
                    .where('student').equal(student)
                    .and('parentCourse').equal(course.id)
                    .orderByDescending('gradeYear').thenByDescending('gradePeriod')
                    .getItems();
                const passed = courseParts.filter(x => {
                    return x.isPassed;
                });
                if (passed.length === courseParts.length) {
                    //calculate complex grade
                    //get complexCoursesDecimalDigits from student department
                    const department = await context.model('Student').where('id').equal(student).select('department').silent().value();
                    const complexCoursesDecimalDigits = await context.model('LocalDepartment').where('id').equal(department).select('complexCoursesDecimalDigits').silent().value();

                    const percent = courseParts.reduce((partial_sum, a) => partial_sum + a.coursePercent, 0);
                    let finalGrade = percent > 0 ? round(courseParts.reduce((partial_sum, a) => partial_sum + a.grade * a.coursePercent, 0) / percent, complexCoursesDecimalDigits +1) : 0;
                    const gradeScale = context.model('GradeScale').convert(course.gradeScale);
                    if (gradeScale.scaleType !== 0) {
                        //use gradeScale to get the exact value for gradeScales with values e.g. 0-10 step 0.5
                        finalGrade = gradeScale.convertTo(finalGrade);
                        finalGrade = gradeScale.convertFrom(finalGrade);
                    }
                    result.grade = finalGrade;
                    result.gradeYear = courseParts[0].gradeYear;
                    result.gradePeriod = courseParts[0].gradePeriod;
                    return result;
                }

                return result;
            }
        } catch (e) {
            throw (e);
        }
    }
}
module.exports = StudentCourse;
