import {EdmMapping} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

@EdmMapping.entityType('StudentThesis')
    /**
     * @class
     * @augments DataObject
     */
class StudentThesis extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    async calculateGrade() {
        const self = this, context = self.context;
        try {
            // get student
            const student = self.student.id || self.student;
            const thesisId = self.thesis.id | self.thesis;
            // get grades for student
            // get thesis results and calculate final grade
            let results = await context.model('StudentThesisResult')
                .where('student').equal(student)
                .and('thesis').equal(thesisId)
                .getItems();
            const passed = results.filter(x => {
                return x.isPassed;
            });
            if (passed.length === results.length) {
                //get thesis committee
                const committee = await context.model('ThesisCommitteeItem').where('thesis').equal(thesisId).getItems();
                // map committee instructor with results
                results = results.map(x => {
                    const committeeItem = committee.find(y => {
                        return y.instructor === x.instructor;
                    });
                    if (committeeItem) {
                        x.factor = committeeItem.factor;
                    } else {
                        x.factor = 0;
                    }
                    return x;
                });
                const thesis = await context.model('Thesis').where('id').equal(thesisId).expand('gradeScale').getItem();
                const percent = results.reduce((partial_sum, a) => partial_sum + a.factor, 0);
                let finalGrade = percent > 0 ? results.reduce((partial_sum, a) => partial_sum + a.grade * a.factor, 0) / percent : 0;
                const gradeScale = await context.model('GradeScale').where('id').equal(thesis.gradeScale).expand('values').getTypedItem();
                //use gradeScale to get the exact value for gradeScales with values e.g. 0-10 step 0.5
                finalGrade = gradeScale.convertTo(finalGrade);
                finalGrade = gradeScale.convertFrom(finalGrade);
                return finalGrade;
            }
            return null;
        } catch (e) {
            throw (e);
        }
    }
}
module.exports = StudentThesis;
