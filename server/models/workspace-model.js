import { EdmMapping, EdmType, DataObject } from '@themost/data';
import localeCode from 'iso-639-1';
/**
 * @class

 */
@EdmMapping.entityType('Workspace')
class Workspace extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.func('locales', EdmType.CollectionOf('Object'))
    static async getLocales(context) {
        const locales = context.getConfiguration().getSourceAt('settings/i18n/locales');
        if (locales == null) {
            return [];
        }
        return locales.map((locale) => {
            return {
                id: locale,
                name: localeCode.getNativeName(locale)
            }
        })
    }

}
module.exports = Workspace;
