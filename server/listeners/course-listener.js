import {DataError} from "@themost/common";
import {DataModel} from "@themost/data";

class CourseListener {
    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {

        const target = event.model.convert(event.target);
        const context = event.model.context;
        const previous =event.previous;

        // get structure type
        const courseStructureType = event.target.courseStructureType;
        if (courseStructureType == null && event.state===2) {
           //load from course
            const structureType = await target.property('courseStructureType').getItem();
            target.courseStructureType = structureType;
        }
        //rule 1 : cannot change courseStructureType
        if (previous && (previous.courseStructureType != target.courseStructureType)) {
            throw new DataError('ERR_INVALID_DATA',
                context.__('Structure type cannot be changed')
                , null,
                'Course');
        }
        // rule 2: remove courseParts from update
        if ( event.state===2 && target.courseParts && target.courseParts.length>0) {
            // remove target.courseParts
            delete event.target.courseParts;
        }
        // get replacedCourse
        const replacedCourse = event.target.replacedCourse;
        // get replacedByCourse
        const replacedByCourse = event.target.replacedByCourse;
        // get previousReplacedCourse
        const previousReplacedCourse = event.previous && event.previous.replacedCourse;
        if (replacedCourse && replacedCourse.length !== 0) {
            // course parts cannot replace other courses
            if (courseStructureType === 8) {
                throw new DataError('ERR_INVALID_DATA',
                context.__('Course parts cannot replace other courses')
                , null,
                'Course');
            }
            // if the course has already replaced other courses, throw error
            if (replacedByCourse) {
                throw new DataError('ERR_INVALID_DATA',
                context.__('Replaced course cannot replace other courses')
                , null,
                'Course');
            }
            if (Array.isArray(replacedCourse)) {
                // if replacedCourse comes as an array, convert it to appropriate format
                const replacedCourseFormatted = replacedCourse.join(',');
                event.target.replacedCourse = replacedCourseFormatted;
            }
            // disabled courses cannot replace other courses
            if ((event.target.isEnabled && event.target.isEnabled === false && event.previous.isEnabled && event.previous.isEnabled === false) && (previousReplacedCourse !== event.target.replacedCourse)) {
                throw new DataError('ERR_INVALID_DATA',
                context.__('Disabled courses cannot replace other courses')
                , null,
                'Course');
            }
        }
        if (replacedByCourse) {
            // course parts cannot be replaced
            if (courseStructureType === 8) {
                throw new DataError('ERR_INVALID_DATA',
                context.__('Course parts cannot be replaced')
                , null,
                'Course');
            }
            if (event.target.isEnabled == true) {
                // disable course
                event.target.isEnabled = false;
            }
        }
    }

    /**
     * @async
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        if (event.state===1) {
            if (event.target.courseParts && event.target.courseParts.length > 0 && event.target.courseStructureType ===4) {

                // check if coursePart exists and throw error
                const courseIds = event.target.courseParts.map(coursePart => {
                    return coursePart.id;
                });
                if (courseIds) {
                    const courses = await context.model('Course').where('id').in(courseIds).getItems();
                    if (courses.length > 0) {
                        throw new DataError('ERR_INVALID_DATA',
                            context.__('Course is not available for coursePart')
                            , null,
                            'Course');
                    }
                }
                let i = 0;
                event.target.courseParts.forEach(course => {
                    i += 1;
                    course.id = course.id ||  `${event.target.id}` + i;
                    course.courseStructureType = 8;
                    course.department = event.target.department;
                    course.gradeScale = event.target.gradeScale;
                    course.parentCourse = event.target.id;
                    course.instructor = course.instructor || event.target.instructor;
                    course.$state = 1;
                });
                await context.model('Course').save(event.target.courseParts);
            }
        }
        if (event.state === 2) {
            // get replacedCourse
            const replacedCourses = event.target.replacedCourse;
            const replacedCoursesPrev = event.previous.replacedCourse;
            if (replacedCourses) {
                if (!replacedCoursesPrev || replacedCoursesPrev != replacedCourses) {
                    const replacedCourse = replacedCourses.split(',').map(id => id.trim());
                    let courses = await context.model('Course').where('id').in(replacedCourse).getItems();
                    let toUpdate = false;
                    let diff = [];
                    if (replacedCoursesPrev) {
                        const replacedCoursePrev = replacedCoursesPrev.split(',').map(id => id.trim()); 
                        diff = replacedCoursePrev.filter(courseId => {
                            return replacedCourse.indexOf(courseId) < 0;
                        });
                    }
                    courses.forEach(course => {
                        if (course.replacedByCourse != event.target.id) {
                            // if replacedByCourse has not already been set, set it
                            course.replacedByCourse = event.target.id;
                            toUpdate = true;
                        }
                    });
                    if (diff && diff.length !== 0) {
                        const coursesDiff = await context.model('Course').where('id').in(diff).getItems();
                        coursesDiff.forEach(course => {
                            course.replacedByCourse = null;
                        });
                        if (toUpdate) {
                            courses = courses.concat(coursesDiff);
                        } else {
                            await context.model('Course').save(coursesDiff);
                        }
                    }
                    if (toUpdate) {
                        await context.model('Course').save(courses);
                    }
                }
            } else {
                // if replacedCourse is null, but it is present in the previous state
                if (replacedCoursesPrev) {
                    const replacedCoursePrevious = replacedCoursesPrev.split(',').map(id => id.trim());
                    const courses = await context.model('Course').where('id').in(replacedCoursePrevious).getItems();
                    courses.forEach(course => {
                        // reset replacedByCourse fields
                        course.replacedByCourse = null;
                    });
                    await context.model('Course').save(courses);
                }
            }
            // get replacedByCourse
            const replacedByCourse = event.target.replacedByCourse;
            const replacedByCoursePrev = event.previous.replacedByCourse;
            if (replacedByCourse) {
                if (!replacedByCoursePrev || replacedByCoursePrev != replacedByCourse) {
                    const course = await context.model('Course').where('id').equal(replacedByCourse).getItem();
                    if (course.replacedCourse) {
                        const courseArray = course.replacedCourse.split(',').map(id => id.trim());
                        const findIndex = courseArray.findIndex(courseId => courseId == event.target.id);
                        if (findIndex < 0) {
                            // update replacedCourse field of the course that replaces the current
                            courseArray.push(event.target.id.toString());
                            course.replacedCourse = courseArray.join(',');
                        }
                    } else {
                        // update replacedCourse field of the course that replaces the current
                        course.replacedCourse = event.target.id.toString();
                    }
                    await context.model('Course').save(course);
                }
            } else {
                // if replacedByCourse is null, but it is present in the previous state
                if (replacedByCoursePrev) {
                    const course = await context.model('Course').where('id').equal(replacedByCoursePrev).getItem();
                    if (course.replacedCourse) {
                        const courseArray = course.replacedCourse.split(',').map(id => id.trim());
                        const findIndex = courseArray.findIndex(courseId => courseId == event.target.id);
                        let toUpdate = false;
                        if (findIndex > -1) {
                            // remove course from replacedCourse field of the course that replaced it
                            courseArray.splice(findIndex, 1);
                            if (courseArray.length === 0) {
                                course.replacedCourse = null;
                            } else {
                                course.replacedCourse = courseArray.join(',');
                            }
                            toUpdate = true;
                        }
                        if (toUpdate) {
                            await context.model('Course').save(course);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async beforeRemoveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        const model = event.model;
        const getReferenceMappings = DataModel.prototype.getReferenceMappings;
        model.getReferenceMappings = async function () {
            const res = await getReferenceMappings.bind(this)();
            // remove readonly model CourseLocale and rules from mapping before delete
            const mappings = ['CourseExamStudentGrade', 'StudentAvailableClass',
                'CourseLocale', 'CourseAreaRuleEx',
                'CourseCategoryRuleEx', 'CourseSectorRuleEx',
                'CourseRuleEx', 'CourseTypeRuleEx',
                'MeanGradeRuleEx', 'ProgramGroupRuleEx', 'RegisteredCourseRuleEx', 'YearMeanGradeRuleEx'];
            return res.filter((mapping) => {
                return mappings.indexOf(mapping.childModel) < 0;
            });
        };
        // if course is complex try to remove course parts
        const course = await context.model('Course')
            .where('id').equal(event.target.id)
            .and('courseStructureType').equal(4)
            .select('id', 'courseStructureType')
            .expand('courseParts')
            .silent()
            .getItem();
        if (course && Array.isArray(course.courseParts) && course.courseParts.length) {
            await context.model('Course').remove(course.courseParts.map(x=>{
                return {id: x.id};
            }));
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return CourseListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return CourseListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return CourseListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
