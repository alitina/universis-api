import {DataEventArgs} from "@themost/data";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    if (event.state !== 2) {
        return callback();
    }
    // execute async method
    return AfterCompleteTeachingEventListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

export class AfterCompleteTeachingEventListener {
    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {
        if(event.state === 2) {
            event.previous.eventStatus = await event.model.context.model('EventStatusType').where('id').equal(event?.previous?.eventStatus?.id ?? event.previous.eventStatus).getItem();
            event.target.eventStatus = await event.model.context.model('EventStatusType').where('id').equal(event?.target?.eventStatus?.id ?? event.target.eventStatus).getItem();
            if (event.target.hasOwnProperty('eventStatus') && event.target.eventStatus && event.target.eventStatus.hasOwnProperty('id') && event.target.eventStatus.id !== event.previous.eventStatus.id) {
                let addAbsences = event.previous.eventStatus.alternateName !== 'EventCompleted' && event.target.eventStatus.alternateName === 'EventCompleted';
                const context = event.model.context;
                let teachingEvent = await context.model('TeachingEvent').where('id').equal(event.target.id).expand(
                    {
                        'name': 'courseClass',
                        'options': {
                            '$expand': 'students'
                        }
                    },
                    {'name': 'eventStatus'},
                    {'name': 'superEvent'},
                    {'name': 'attendanceList'}
                ).silent().getItem();
                if (teachingEvent.hasOwnProperty('courseClass') && teachingEvent.courseClass.hasOwnProperty('students') && Array.isArray(teachingEvent.courseClass.students)) {
                    let studentAbsences = [];
                    const students = teachingEvent.courseClass.students;
                    const attendanceList = teachingEvent.attendanceList;
                    for (const attendance of attendanceList) {
                        const student = students.find(x => {
                            return x.student === attendance.student;
                        });
                        if (student) {
                            const studentCourseClass = await context.model('StudentCourseClass')
                                .where('student').equal(student.student)
                                .and('courseClass').equal(teachingEvent.courseClass.id)
                                .silent().getItem();
                            if (studentCourseClass && studentCourseClass.hasOwnProperty('absences')) {
                                if (addAbsences) {
                                    studentCourseClass.absences += !teachingEvent.presenceType ? teachingEvent.eventAttendanceCoefficient : 0;
                                } else {
                                    studentCourseClass.absences -= !teachingEvent.presenceType ? teachingEvent.eventAttendanceCoefficient : 0;
                                }
                                studentAbsences.push({...studentCourseClass, $state: 2});
                            }
                        }
                    }
                    await context.model('StudentCourseClass').silent().save(studentAbsences)
                }
            }
        }
    }
}
