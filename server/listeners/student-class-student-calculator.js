import _ from 'lodash';
import {StudentNotFound} from "../errors";

/**
 * @param {DataEventArgs} event
 * @param {function(Error|*=)} callback
 */
export function beforeSave(event, callback) {
    try {
        const context = event.model.context,
              /**
               * @type {{registration:*, student:*}}
               */
              target = event.target;
        if (target.hasOwnProperty('student')) { return callback(); }
        if (target.hasOwnProperty('registration')) {
            const registration = context.model('StudentPeriodRegistration').convert(target.registration);
            if (_.isObject(registration)) {
                registration.property('student').select('id').asArray().first(function(err, student) {
                    if (err) { return callback(err); }
                    if (student) {
                        target.student = student;
                    }
                    else {
                        callback(new StudentNotFound());
                    }
                });
            }
            else {
                callback(new Error('Student registration data cannot be empty at this context'));
            }
        }
        else {
            callback(new StudentNotFound());
        }
    }
    catch (e) {
        callback(e)
    }
}