/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {DataError} from "@themost/common";
import ActionStatusType from '../models/action-status-type-model';
// eslint-disable-next-line no-unused-vars
import Student from '../models/student-model';
import * as _ from 'lodash';

export function beforeSave(event, callback) {
    (async () => {
        // validate student state (on insert or update)
        // todo:: move this action to an upper level
        if (event.state === 1) {
            let context = event.model.context;
            /**
             * @type {Student|DataObject|*}
             */
            const student = context.model('Student').convert(event.target.object);
            const isActive = await student.silent().isActive();
            if (!isActive) {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student suspend action may execute upon an active student only.'
                    , null,
                    'Student', 'studentStatus');
            }
            //check if there is an active student suspend action for this student
            const action = await context.model(event.model.name).where('object').equal(student.id).and('actionStatus/alternateName').equal('ActiveActionStatus').silent().count();
            if (action) {
                throw new DataError('ERR_ACTION_EXISTS',
                    'There is already an active suspend action.'
                    , null,
                    'Student', 'studentStatus');
            }
        }
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        const context = event.model.context;
        let target;
        // get own properties
        const attributes = event.model.attributes.filter( attribute => {
            if (attribute.primary) {
                return false;
            }
            return attribute.model === event.model.name;
        }).map( attribute => {
            return attribute.name;
        });
        // on insert
        if (event.state === 1) {
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();
            // TODO: calculate identifier
            // validate completed state
            if (target.actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
                return;
            }
        }
        // on update
        else if (event.state === 2) {
            // get previous state
            const previousActionStatus = event.previous.actionStatus;
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().getTypedItem();
            if ((previousActionStatus.alternateName === ActionStatusType.ActiveActionStatus &&
                target.actionStatus.alternateName === ActionStatusType.CompletedActionStatus) === false) {
                return;
            }
        }
        else {
            return;
        }
        // do student suspension (patch student)
        // pick student suspension attributes
        // add suspension
        const studentSuspension = _.pick(target, attributes);
        // set student attribute
        studentSuspension.student = target.object.id;
        studentSuspension.reintegrated = 0;
        // calculate identifier
        const totalSuspensions = await context.model('StudentSuspension').where('student').equal(target.object.id).silent().count();
        studentSuspension.identifier = `${target.object.id}_${totalSuspensions +1}`;
        // save student suspension
        const result = await context.model('StudentSuspension').silent().save(studentSuspension);


        // assign id and studentStatus
       const student =  {
            id: target.object.id,
            studentStatus: {
                alternateName: 'suspended'
            }};
        // update student status
        await context.model('Student').silent().save(student);

        // save target.studentSuspension to action
        //connect action with student suspension
        target.studentSuspension =  result.id;
        await context.model('StudentSuspendAction').silent().save(target);


    })().then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
