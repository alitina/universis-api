import _ from 'lodash';
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    try {
        const context = event.model.context;
        let error;
        const persons = context.model('Person');
        //get person only data
        const person = persons.cast(event.target);
        if (event.state===1) {
            //set person id, if any
            delete person.id;
            if (!_.isNil(event.target._id)) {
                person.id = event.target._id;
            }
            persons.save(person, function(err) {
                if (err) { return callback(err); }
                event.target._id = person.id;
            });
        }
        else if (event.state===2) {
            event.model.where('id').equal(event.target.id).select('_id').silent().flatten().first(function(err, result) {
                if (err) { return callback(err); }
                if (_.isNil(result)) {
                    error = new Error('Person data cannot be found.');error.code = 'EDATA';
                    return callback(error);
                }
                if (_.isNil(result._id)) {
                    error = new Error('Invalid person data.');error.code = 'EDATA';
                    return callback(error);
                }
                //set person id to target
                event.target._id = result._id;
                //safely delete person id attribute (from person object)
                delete person.id;
                //and set the one that was found
                person.id = event.target._id;
                //save person
                persons.save(person, function(err) {
                    if (err) { return callback(err); }
                    //safely set person id to target object after save
                    event.target._id = person.id;
                    callback();
                });
            });
        }
        else {
            callback();
        }
    }
    catch (e) {
        callback(e)
    }
}