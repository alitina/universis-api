import {DataEventArgs} from "@themost/data";
import {DataError} from "@themost/common";
import {HttpServerError} from "@themost/common/errors";
import {DataConflictError} from "../errors";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    if (event.state !== 4) {
        return callback();
    }
    return BeforeRemoveTeachingEventListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

export class BeforeRemoveTeachingEventListener {
    /**
     * @param {DataEventArgs} event
     */
    static async beforeRemoveAsync(event) {
        if (event.state === 4) {
            const context = event.model.context;

            let result = await Promise.all([context.model('TeachingEvent').where('id').equal(event.target.id).expand(
                {'name': 'sections'},
                {'name': 'eventStatus'},
                {'name': 'attendanceList'}
            ).silent().getItem(), context.model('TeachingEvent').where('superEvent').equal(event.target.id).silent().getItems()]);
            if (!(result && Array.isArray(result) && result.length)) {
                throw new HttpServerError()
            }
            let teachingEvent = result[0];
            let subevents = result[1];
            if (subevents && Array.isArray(subevents) && subevents.length) {
                throw new DataError('403', 'Cannot remove event that has sub-events. Try removing the one you want.', null, 'TeachingEvent', 'superEvent');
            }
            if (teachingEvent?.attendanceList && teachingEvent?.attendanceList.length) {
                throw new DataConflictError('Cannot remove event for which you have submitted attendance records', null, 'TeachingEvent');
            }
            if (teachingEvent?.eventStatus?.alternateName === 'EventCompleted') {
                throw new DataError('403', 'Cannot remove completed event.', null, 'TeachingEvent', 'eventStatus')
            }
            if (!teachingEvent?.sections) {
                return;
            }
           return await context.model('TeachingEventCourseClassSections').silent().remove(await context.model('TeachingEventCourseClassSections').where('parentId').equal(teachingEvent.id).getItems())
        }
    }
}
