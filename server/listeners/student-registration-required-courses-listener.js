import _ from 'lodash';
import {ValidationResult} from "../errors";
import util from "util";
import {LangUtils, TraceUtils} from "@themost/common";

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {

    const target = event.model.convert(event.target);
    const context = event.model.context;
    let classes;
    if (util.isArray(target['classes'])) {
        classes = target['classes'].filter(function (x) {
            return x.$state !== 4;
        });
        if (classes.length === 0) {
            return;
        }
        // get student
        const studentId = event.model.idOf(event.target["student"]);
        const student = await context.model('Student').where('id').equal(studentId).select('id', 'department', 'studyProgram', 'semester', 'specialtyId').flatten().silent().getTypedItem();

        // this rule will be executed only if checkCustomRules is set to true, this will be changed later on.
        const department = await context.model('LocalDepartment').where('id').equal(student.department).select('checkCustomRules').first();
        if (LangUtils.parseInt(department['checkCustomRules']) === 0) {
            return;
        }

        // get all department rules associated with courseTypes
        const departmentRules = await context.model('RequiredCourseTypeRule').where('target').equal(student.department)
            .and('targetType').equal('Department')
            .and('refersTo').equal('CourseType')
            .and('additionalType').equal('RequiredCourseTypeRule')
            .silent().getItems();

        if (departmentRules && departmentRules.length > 0) {
            let checkValues = [];
            for (let i = 0; i < departmentRules.length; i++) {
                if (!_.isNil(departmentRules[i].checkValues)) {
                    checkValues = checkValues.concat(departmentRules[i].checkValues.split(','));
                }
            }
            if (checkValues.length === 0) {
                return;
            }
            const requiredCourseTypes = await context.model('CourseType').where('id').in(checkValues).getItems();
            //get all available courses for courseTypes
            const studentCourses = await (await student.getAvailableClasses())
                .where('courseType').in(checkValues)
                .and('semester').lowerOrEqual(student.semester)
                .select('course')
                .getItems();

            // get not passed student courses that are not included in current registration for specified courseTypes and with semester lower or equal student semester
            /* const studentCourses = await context.model('StudentCourse').where('student').equal(studentId)
                 .and('courseType').in(checkValues)
                 .and('isPassed').equal(false)
                 .and('semester').lowerOrEqual(student.semester)
                 .select('course')
                 .flatten().getItems();*/

            if (studentCourses.length === 0) {
                return;
            }

            let requiredCourses = [];

            // if required course is not included in current registration add this to requiredCourses
            studentCourses.forEach(studentCourse => {
                const isRegistered = classes.find((x) => {
                    return x.course === studentCourse.course;
                });
                if (!isRegistered) {
                    requiredCourses.push(studentCourse.course);
                }
            });

            if (requiredCourses.length === 0) {
                return;
            }
            // check if required courses are available, get available classes
            // get available classes for required courses
            const studentAvailableClasses = await (await student.getAvailableClasses()).where('course').in(requiredCourses).getItems();
            if (studentAvailableClasses.length === 0) {
                return;
            }

            // validate required courses - only available courses should be registered
            let coursesForRegister = [];

            // add async function for validating available class
            let forEachCourse = async availableClass => {
                try {
                    const studentCourseClass = context.model('StudentCourseClass').convert(availableClass);
                    //add registrationYear and period
                    studentCourseClass.registrationYear = target.registrationYear;
                    studentCourseClass.registrationPeriod = target.registrationPeriod;
                    return await new Promise((resolve) => {
                        studentCourseClass.validate(function (err, result) {
                            if (err) {
                                result = _.isNil(result)? [] :result;
                                if (err instanceof ValidationResult) {
                                    result.push(err);
                                } else {
                                    TraceUtils.error(err);
                                    result.push(new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message));
                                }
                            }
                            const success = (typeof result.find(function (x) {
                                return !x.success
                            }) === 'undefined');
                            if (success) {
                                // add to required
                                coursesForRegister.push(studentCourseClass);
                            }
                            return resolve();
                        });
                    });
                } catch (err) {
                    TraceUtils.error(err);
                }
            };
            // call all promises
            await Promise.all(studentAvailableClasses.map(availableClass => {
                return forEachCourse(availableClass);
            }));

            if (coursesForRegister.length > 0) {
                // Not valid.  These courses should be included in registration
                let message='The courses of types (%s) should be included into your registration';
                message =context.__(message);
                const args = [];
                const courseTypes=requiredCourseTypes.map(function(x) { return `${x.name}`; }).join(',');
                args.push(courseTypes);
                args.unshift(message);

                const courses= coursesForRegister.map(function(x) { return `(${x.displayCode}) ${x.name}`; }).join(',');
                message= util.format.apply(this,args);

                // return validationResult
                const validationResult = Object.assign(new ValidationResult(false, 'EFAIL', message, courses, coursesForRegister), {
                    type: 'RequiredCourseTypeRule'
                });
                return validationResult;
            }
        }
        return;
    }
    return;
}


export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    if (event.state !== 2 && event.state!==1) {
        return callback();
    }
    // execute async method
    return afterSaveAsync(event).then((validationResult) => {
        event.target.validationResult = validationResult;
        if (validationResult && !validationResult.success)
        {
            return callback(validationResult);
        }
        return callback();
    }).catch(err => {
        return callback(err);
    });
}