import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import util from "util";
import Rule from "./../models/rule-model";
import {ValidationResult} from "../errors";
import {EdmMapping} from "@themost/data";

@EdmMapping.entityType('Rule')
/**
 * @class CourseRule
 * @constructor
 * @augments Rule
 * @property {string} checkValues - A concatenated string with the courses that are going to be checked by this rule
 * @property {string} ruleOperator
 * @property {string} value1
 * @property {string} value2
 * @property {string} value3
 * @property {string} value4
 * @property {string} value5
 * @property {number} minExam
 * @property {number} maxExam
 */
class CourseRule extends Rule {
    constructor() {
        super();

    }

    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {Function} done
     */
    validate(obj, done) {
        const self = this, context = self.context;
        try {
            if (_.isNil(this.checkValues)) {
                return done(null, new ValidationResult(true));
            }
            if (_.isNil(obj)) {
                return done(null, new ValidationResult(true));
            }
            if (_.isNil(obj.student)) {
                return done(null, self.failure('ESTUD','Student data is missing.'));
            }
            /**
             * @type {DataModel}
             */
            const students = context.model('Student');
            /**
             * Represents the student associated with the given data
             * @type {Student|*}
             */
            const student = students.convert(obj.student);
            /**
             * Represents an array of prerequisites for the given data
             * @type {Array}
             */
            const values = this.checkValues.split(',');
            /**
             * Initialize the data queryable associated with the given rule
             * @type {DataQueryable|QueryExpression}
             */
            const q = context.model('StudentCourse').where('student').equal(student.getId()).and('course').in(values).prepare();
            //get operator
            const op = this.operatorOf();
            if (_.isNil(op)) {
                return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.', 'The specified operator is not yet implemented.'));
            }
            const fnOperator = q[op];
            if (typeof fnOperator !== 'function') {
                return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.', 'The specified operator cannot be found or is invalid.'));
            }
            //apply operator expression
            q.where('grade');
            fnOperator.call(q, LangUtils.parseFloat(this.value3));
            q.or('registrationType').equal(1).prepare();

            self.excludeStudent(student, function(err, exclude) {
                if (err) {
                    TraceUtils.error(err);
                    return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.', 'An occurred while searching excluded students.'));
                }
                if (exclude) {
                    return done(null, self.success('SUCC','The input data meets the specified rules.','Student was excluded from rule validation due to specific attributes.'));
                }
                else {
                    q.silent().count(function(err, count) {
                        if (err) {
                            return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.', 'An error occured while searching courses.'));
                        }
                        // keep result in data and pass this to validationResult
                        const data= {
                            "result": count,
                            "operator": op,
                            "value1": self.value3,
                            "value2": self.value4
                        };
                        //get minimum of prerequisites
                        const minCount = LangUtils.parseInt(self.value4);
                        self.courses(function(err, result) {
                            if (err) {
                                TraceUtils.error(err);
                                result = [];
                            }
                            let message = self.formatMessage(result.map(function(x) { return x.description; }).join(', '));
                            if (self.value3 != result[0].base) {
                                // add also grade to message
                                try {
                                    //convert gradeScale
                                    /**
                                     *
                                     * @type {GradeScale}
                                     */
                                    const gradeScale = context.model('GradeScale').convert(result[0].gradeScale);
                                    message +=` (${context.__('with grade')} ${gradeScale.convertTo(LangUtils.parseFloat(self.value3))})`;
                                } catch (e) {
                                    //
                                }
                            }
                            if (minCount<=count) {
                                //success
                                return done(null, self.success('SUCC',message,null,data));
                            }
                            else {
                                return done(null, self.failure('FAIL',message, null,data));
                            }
                        });
                    });
                }
            });
        }
        catch(e) {
            done(e);
        }
    }

    courses(callback) {
        try {
            const values = this.checkValues.split(',');
            this.context.model('Course').where('id').in(values).select('id','displayCode','name','gradeScale/scaleBase as base','gradeScale').silent().all((err, result)=> {
               if (err) { return callback(err); }
                result.forEach(function(x) {
                    x.description = `(${x.displayCode}) ${x.name}`;
                });
                callback(null, result);
            });
        }
        catch(e) {
            callback(e);
        }
    }

    /**
     * @returns {*} ...arg
     */
    formatMessage() {
        //get number of passed courses
        let s="";
        const minCount = LangUtils.parseInt(this.value4);
        if (minCount===1) {
            s = 'The prerequisite course must be passed.'
        }
        else {
            s = '%s courses of the following prerequisite courses %s must be passed.'
        }
        const args = [].slice.call(arguments);
        args.unshift(minCount);
        args.unshift(this.context.__(s));
        return util.format.apply(this,args);
    }
}
module.exports = CourseRule;
