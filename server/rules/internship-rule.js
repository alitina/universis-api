import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import util from "util";
import { QueryExpression } from '@themost/query/query';
import Rule from "./../models/rule-model";

/**
 * @class
 * @augments Rule
 */
export default class InternshipRule extends Rule {
    constructor(obj) {
        super('Rule', obj);
    }

    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {function(Error=,*=)} done
     */
    validate(obj, done) {
        try {
            //done(null, new ValidationResult(false,'FAIL','Class Registration was cancelled by the user'));
            const self = this;

            const context = self.context;
            let res;
            const students = context.model('Student'), student = students.convert(obj.student), internship=context.model('Internship');
            if (_.isNil(student)) {
                return done(null, self.failure('ESTUD','Student data is missing.'));
            }
            if (_.isNil(student.getId())) {
                return done(null, self.failure('ESTUD','Student data cannot be found.'));
            }
            const op = this.operatorOf();
            if (_.isNil(op)) {
                return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.','The specified operator is not yet implemented.'));
            }
            //get queryable object completed thesis
            const q = internship.where('student').equal(student.getId()).and('status').equal(3).prepare();
            let fnOperator = q[op];
            if (typeof fnOperator !== 'function') {
                return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.', 'The specified operator cannot be found or is invalid.'));
            }
            const fieldOf="count(student) as res";
            q.silent().select(fieldOf).first(function(err, result) {
                if (err) {
                    TraceUtils.error(err);
                    return done(null, self.failure('EFAIL', 'An error occurred while trying to validate rules.'));
                }
                const q2 = (new QueryExpression().from('a')).select([
                    { res: { $value: LangUtils.parseFloat(result.res) }}
                ]).where('res');
                q2.$fixed = true;
                fnOperator = q2[op];
                fnOperator.apply(q2, [ LangUtils.parseFloat(self.value1), LangUtils.parseFloat(self.value2) ]);
                internship.context.db.execute(q2, null, function (err, result) {
                    if (err) {
                        return done(err);
                    }
                    else {
                        // keep result in data and pass this to validationResult
                        const data= {
                            "result": result && result.length,
                            "operator": op,
                            "value1": self.value1,
                            "value2": self.value2
                         };
                        if (result.length === 1) {
                            self.formatDescription(function(err, message) {
                                if (err) { return done(err); }
                                return done(null, self.success('SUCC',message,null, data));
                            });
                        }
                        else {
                            self.formatDescription(function(err, message) {
                                if (err) { return done(err); }
                                return done(null, self.failure('FAIL',message, null, data));
                            });
                        }
                    }
                });

            });
        }
        catch(e) {
            done(e);
        }
    }

    formatDescription(callback) {
        const op = LangUtils.parseInt(this.ruleOperator);
        let s;
        const fieldDescription="Number of completed internships";
        const args=[];
        switch (op) {
            case 0:s = "%s must be equal to %s"; break;
            case 1:s = "%s must contain '%s'"; break;
            case 2: s = "%s must be different from %s"; break;
            case 3: s = "%s must be greater than %s"; break;
            case 4: s = "%s must be lower than %s"; break;
            case 5: s = "%s must be between %s and %s"; break;
            case 6: s = "%s must start with '%s'"; break;
            case 7: s = "%s must not contain '%s'"; break;
            case 8: s = "%s must be greater or equal to %s"; break;
            case 9: s = "%s must be lower or equal to %s"; break;
        }
        s=this.context.__(s);
        args.push(this.context.__(fieldDescription));
        args.push(this.value1);
        args.push(this.value2);
        for (let i = 0; i < args.length; i++) {
            if (_.isNil(args[i])) {
                args[i]='';
            }
        }
        args.unshift(s);
        s= util.format.apply(this,args);
        callback(null,s);
    }
}
