import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import {createStudyProgram} from './data';
import Course from "../server/models/course-model";
import StudyProgramCourse from "../server/models/study-program-course-model";
import SpecializationCourse from "../server/models/specialization-course-model";
const executeInTransaction = TestUtils.executeInTransaction;

describe('SpecializationCourse', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        // disable SQL logging
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        // re-enable development environment
        process.env.NODE_ENV = 'development';
        return done();
    });

    it('should create specialization course', async () => {
        await executeInTransaction(context, async () => {
            const studyProgram = await createStudyProgram(context);
            // get course
            const course = await context.model(Course)
                .where('displayCode').equal('L100')
                .silent().getItem();
            expect(course).toBeTruthy();
            let studyProgramCourse = {
                course: course,
                studyProgram: studyProgram,
                sortIndex: 1
            };
            await context.model(StudyProgramCourse)
                .silent().save(studyProgramCourse);
            studyProgramCourse = await context.model(StudyProgramCourse)
                .where('course').equal(course.id)
                .and('studyProgram').equal(studyProgram.id)
                .silent().getItem();
            // get core specialization
            const specialization = studyProgram.specialties.find( x=> {
               return x.specialty = -1;
            });
            /**
             *
             * @type {SpecializationCourse}
             */
            let specializationCourse = {
                specialization: specialization,
                studyProgramCourse: studyProgramCourse,
                semester: 1,
                coefficient: 1,
                courseType: 1,
                sortIndex: 1,
                ects: 4,
                units: 4
            };
            await context.model(SpecializationCourse).silent().insert(specializationCourse);

            specializationCourse = await context.model(SpecializationCourse)
                .where('specialization').equal(specialization.id)
                .and('studyProgramCourse').equal(studyProgramCourse.id)
                .silent()
                .getItem();
            expect(specializationCourse).toBeTruthy();
            expect(specializationCourse.specialization).toBe(specialization.id);
            expect(specializationCourse.specializationIndex).toBe(specialization.specialty);
        });
    });

    it('should update specialization course', async () => {
        await executeInTransaction(context, async () => {
            const studyProgram = await createStudyProgram(context);
            // get course
            const course = await context.model(Course)
                .where('displayCode').equal('L100')
                .silent().getItem();
            expect(course).toBeTruthy();
            let studyProgramCourse = {
                course: course,
                studyProgram: studyProgram,
                sortIndex: 1
            };
            await context.model(StudyProgramCourse)
                .silent().save(studyProgramCourse);
            studyProgramCourse = await context.model(StudyProgramCourse)
                .where('course').equal(course.id)
                .and('studyProgram').equal(studyProgram.id)
                .silent().getItem();
            // get core specialization
            const specialization = studyProgram.specialties.find( x=> {
                return x.specialty = -1;
            });
            /**
             *
             * @type {SpecializationCourse}
             */
            let specializationCourse = {
                specialization: specialization,
                studyProgramCourse: studyProgramCourse,
                semester: 1,
                coefficient: 1,
                courseType: 1,
                sortIndex: 1,
                ects: 4,
                units: 4
            };
            await context.model(SpecializationCourse).silent().insert(specializationCourse);

            specializationCourse = await context.model(SpecializationCourse)
                .where('specialization').equal(specialization.id)
                .and('studyProgramCourse').equal(studyProgramCourse.id)
                .silent()
                .getItem();

            specializationCourse.sortIndex = 100;
            await context.model(SpecializationCourse).silent().save(specializationCourse);

            specializationCourse = await context.model(SpecializationCourse)
                .where('specialization').equal(specialization.id)
                .and('studyProgramCourse').equal(studyProgramCourse.id)
                .silent()
                .getItem();

            expect(specializationCourse).toBeTruthy();
            expect(specializationCourse.sortIndex).toBe(100);
            expect(specializationCourse.specialization).toBe(specialization.id);
            expect(specializationCourse.specializationIndex).toBe(specialization.specialty);
        });
    });

    it('should delete specialization course', async () => {
        await executeInTransaction(context, async () => {
            const studyProgram = await createStudyProgram(context);
            // get course
            const course = await context.model(Course)
                .where('displayCode').equal('L100')
                .silent().getItem();
            expect(course).toBeTruthy();
            let studyProgramCourse = {
                course: course,
                studyProgram: studyProgram,
                sortIndex: 1
            };
            await context.model(StudyProgramCourse)
                .silent().save(studyProgramCourse);
            studyProgramCourse = await context.model(StudyProgramCourse)
                .where('course').equal(course.id)
                .and('studyProgram').equal(studyProgram.id)
                .silent().getItem();
            // get core specialization
            const specialization = studyProgram.specialties.find( x=> {
                return x.specialty = -1;
            });
            /**
             *
             * @type {SpecializationCourse}
             */
            let specializationCourse = {
                specialization: specialization,
                studyProgramCourse: studyProgramCourse,
                semester: 1,
                coefficient: 1,
                courseType: 1,
                sortIndex: 1,
                ects: 4,
                units: 4
            };
            await context.model(SpecializationCourse).silent().insert(specializationCourse);

            await context.model(SpecializationCourse).silent().remove(specializationCourse);

            specializationCourse = await context.model(SpecializationCourse)
                .where('specialization').equal(specialization.id)
                .and('studyProgramCourse').equal(studyProgramCourse.id)
                .silent()
                .getItem();
            expect(specializationCourse).toBeFalsy();
        });
    });
});
