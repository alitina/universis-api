import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import Institute from "../server/models/institute-model";
import Department from "../server/models/department-model";
import {INSTITUTE, DEPARTMENT} from './data';
const executeInTransaction = TestUtils.executeInTransaction;

describe('DepartmentReportVariableValue', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        // disable sql logging
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        // re-enable development env
        process.env.NODE_ENV = 'development';
        return done();
    });

    it('should insert report variable value', async () => {
        await executeInTransaction(context, async ()=> {
            await context.model(Institute).silent().insert(INSTITUTE);
            await context.model(Department).silent().insert(DEPARTMENT);
            let reportVariableType = {
                name: 'Established',
                alternateName: 'EstablishedYear',
                typeString: 'Number'
            };
            await context.model('ReportVariableType').silent().save(reportVariableType);
            reportVariableType = await context.model('ReportVariableType').where('name').equal('Established').silent().getItem();
            expect(reportVariableType).toBeTruthy();
            let newReportVariable = {
                department: DEPARTMENT,
                reportVariable: reportVariableType,
                valueNumber: 1998,
                inLanguage: null
            };
            await context.model('DepartmentReportVariableValue').silent().save(newReportVariable);
            let value = await context.model('DepartmentReportVariableValue')
                .where('reportVariable/name').equal('Established')
                .and('department').equal(DEPARTMENT.id)
                .select('valueNumber')
                .silent().value();
            expect(value).toBe(1998);
            await context.model('DepartmentReportVariableValue').silent().save({
                department: DEPARTMENT,
                reportVariable: reportVariableType,
                valueNumber: 1996,
                inLanguage: null
            });
            value = await context.model('DepartmentReportVariableValue')
                .where('reportVariable/name').equal('Established')
                .and('department').equal(DEPARTMENT.id)
                .select('valueNumber')
                .silent().value();
            expect(value).toBe(1996);

        });
    });

    it('should insert report variable value by language', async () => {
        await executeInTransaction(context, async ()=> {
            await context.model(Institute).silent().insert(INSTITUTE);
            await context.model(Department).silent().insert(DEPARTMENT);
            let reportVariableType = {
                name: 'Secretariat Title',
                alternateName: 'SecretariatTitle',
                typeString: 'Text'
            };
            await context.model('ReportVariableType').silent().save(reportVariableType);
            reportVariableType = await context.model('ReportVariableType')
                .where('alternateName').equal('SecretariatTitle')
                .silent().getItem();
            expect(reportVariableType).toBeTruthy();
            let newReportVariable = {
                department: DEPARTMENT,
                reportVariable: reportVariableType,
                valueText: 'Secretariat',
                inLanguage: 'en'
            };
            await context.model('DepartmentReportVariableValue').silent().save(newReportVariable);
            let value = await context.model('DepartmentReportVariableValue')
                .where('reportVariable/alternateName').equal('SecretariatTitle')
                .and('department').equal(DEPARTMENT.id)
                .and('inLanguage').equal('en')
                .select('valueText')
                .silent().value();
            expect(value).toBe('Secretariat');

            await context.model('DepartmentReportVariableValue').silent().save({
                department: DEPARTMENT,
                reportVariable: reportVariableType,
                valueText: 'Γραμματεία',
                inLanguage: 'el'
            });
            let items = await context.model('DepartmentReportVariableValue')
                .where('reportVariable/alternateName').equal('SecretariatTitle')
                .and('department').equal(DEPARTMENT.id)
                .silent().getItems();
            expect(items.length).toBe(2);
            value = items.find(value1 => {
                return value1.inLanguage === 'el';
            });
            expect(value).toBeTruthy();
            expect(value.valueText).toBe('Γραμματεία');

        });
    });

});
