FROM node:12-alpine AS builder
WORKDIR /app

COPY package.json package-lock.json ./

RUN npm ci
COPY . .

RUN npm run build

ENTRYPOINT ["npm", "start"]

FROM node:12-alpine AS final
RUN mkdir -p /app/bin /app/dist /app/locales /app/public && chown -R node:node /app
USER node
ENV NODE_ENV production
ENV IP 0.0.0.0
WORKDIR /app

COPY package*.json ./
RUN npm i --only=production

COPY --chown=node:node --from=builder /app/bin ./bin
COPY --chown=node:node --from=builder /app/dist ./dist
COPY --chown=node:node --from=builder /app/locales ./locales
COPY --chown=node:node --from=builder /app/public ./public
EXPOSE 5001

ENTRYPOINT ["node", "./bin/www"]
